/*
*   jQuery input steps
 *  Copyright (c) 2017 Cassio Vidal (http://www.cassiovidal.com)
 *  Licensed under MIT http://www.opensource.org/licenses/MIT
 */

/*global jQuery*/

(function ($) {
    "use strict";
    $.fn.inputSteps = function (options) {
        //Defaults settings
        var settings = $.extend({
            line: ".row",
            number: ".question_number"
        }, options),
            rows = $(this).children(settings.line),
            first = $(this).children(settings.line).first(),
            allImputs = $(":input"),
            allLabels = $(this).find('label'),
            lastFocus = first.find('input');
    /************************************************************
    | visualização do formulário
    ************************************************************/
        rows.each(function () {
            var control = $(this);
            control.addClass("opacity");
            control.children(settings.number).addClass("question_number_hide");
        });
        first.removeClass("opacity");
        first.children(settings.number).removeClass("question_number_hide");
    /************************************************************
    | ação dos inputs
    ************************************************************/
        allImputs.focus(function () {
            var row = $(this).closest(settings.line),
                lastRow = lastFocus.closest(settings.line);
            if (row.attr('id') !== lastRow.attr('id')) {
                row.removeClass("opacity");
                row.children(settings.number).removeClass("question_number_hide");
                lastRow.addClass("opacity");
                lastRow.children(settings.number).addClass("question_number_hide");
            }
            lastFocus = $(this);
        });
        allLabels.click(function () {
            var row = $(this).closest(settings.line),
                lastRow = lastFocus.closest(settings.line);
            if (row.attr('id') !== lastRow.attr('id')) {
                row.removeClass("opacity");
                row.children(settings.number).removeClass("question_number_hide");
                lastRow.addClass("opacity");
                lastRow.children(settings.number).addClass("question_number_hide");
            }
            lastFocus = $(this);
        });
    };
}(jQuery));
