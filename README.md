#jQuery.input-steps Plugin#
====================

This README would normally document whatever steps are necessary to get your application up and running.

### Descrição ###

Este plugin foi desenvolvido para criar um comportamento semelhante ao jQuery.steps, porém voltado aos inputs. Ele adiciona uma opacidade nos inputs fora de foco. 

No futuro iremos adicionar validação dos dados ao passar em cada input, não permitindo o prosseguimento em caso de falha da validação. 

### Utilização ###

Para utilizar este plugin basta incluir os arquivos da pasta assets no arquivo html e realizar a chamada da função da seguinte forma:

jQuery('identificador_pai').inputSteps({
        line: 'identificador das linhas do formulario',
        number: 'identificador do numero da questão'
    });

o arquivo example.html possui o plugin implementado. 
